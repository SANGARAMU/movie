package com.jspiders.payment.Repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.jspiders.mavenbasics.entity.Payment;
import com.jspiders.mavenbasics.util.sessionUtil;


public class PaymentRepository 
{ 
      public void savePaymentDetails(Payment payment)
      {
    	  payment.setTotalPrice(payment.getPrice()*payment.getNumberOfTicket());
    	  SessionFactory sf= sessionUtil.getInstance();
	  	   Session session = sf.openSession(); 
		   Transaction transaction = session.beginTransaction();
		   session.save(payment);
		   transaction.commit();
      }
    	 
}
