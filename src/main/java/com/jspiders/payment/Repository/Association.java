package com.jspiders.payment.Repository;

import com.jspiders.mavenbasics.entity.Company;
import com.jspiders.mavenbasics.entity.Country;
import com.jspiders.mavenbasics.entity.Department;
import com.jspiders.mavenbasics.entity.School;
import com.jspiders.mavenbasics.entity.Student;

public interface Association {
      public void saveCountryDetails(Country country);
      
      public void saveCompanyDetails(Company company);
      
      public void saveStudentDetails(Student student);
      
      
}
