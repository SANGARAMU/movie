package com.jspiders.payment.Repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.jspiders.mavenbasics.entity.Company;
import com.jspiders.mavenbasics.entity.Country;
import com.jspiders.mavenbasics.entity.Department;
import com.jspiders.mavenbasics.entity.School;
import com.jspiders.mavenbasics.entity.Student;
import com.jspiders.mavenbasics.util.sessionUtil;

public class Associationimp implements Association
{



	public void saveCountryDetails(Country country) {
		SessionFactory sessionFactory=sessionUtil.getInstance();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		session.save(country);
		transaction.commit();
	}

	public void saveCompanyDetails(Company company) {
		SessionFactory sessionFactory=sessionUtil.getInstance();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		session.save(company);
		transaction.commit();
	}

	public void saveStudentDetails(Student student) {
		SessionFactory sessionFactory=sessionUtil.getInstance();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		session.save(student);
		transaction.commit();
		
	}




}
