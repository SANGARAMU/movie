package com.jspiders.mavenbasics.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.mavenbasics.constants.AppConstants;
@Entity
@Table(name=AppConstants.PM_INFO)
public class PrimeMinister implements Serializable
{
	@Id
	@GenericGenerator(name = "p_auto", strategy = "increment")
	@GeneratedValue(generator="p_auto")
    @Column(name="id")
	private Long id;
	
    @Column(name="name")
	private String name;
	
    @Column(name="age")
	private Long age;
	
    @Column(name="address")
	private String address;
	
    @Column(name="qualification")
	private String qualification;
	
    @Column(name="political_party")
	private String politicalParty;
	
	public PrimeMinister() {
		// TODO Auto-generated constructor stub
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getPoliticalParty() {
		return politicalParty;
	}

	public void setPoliticalParty(String politicalParty) {
		this.politicalParty = politicalParty;
	}
	
}
