package com.jspiders.mavenbasics.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jspiders.mavenbasics.constants.AppConstants;
@Entity
@Table(name=AppConstants.PAYMENT_INFO)
public class Payment implements  Serializable
{
	@Id
	@Column(name="id")
     private Long id;
     
	@Column(name="movie_id")
     private Long movieid;
     
	@Column(name="number_of_ticket")
     private Long numberOfTicket;
     
	@Column(name="price")
     private Double price;
     
	@Column(name="show_date")
     private Date showDate;
     
	@Column(name="[show_time")
     private String showTime;
     
	@Column(name="payment_type")
     private String paymentType;
     
	@Column(name="total_price")
     private Double totalPrice;
     
     public Payment() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMovieid() {
		return movieid;
	}

	public void setMovieid(Long movieid) {
		this.movieid = movieid;
	}

	public Long getNumberOfTicket() {
		return numberOfTicket;
	}

	public void setNumberOfTicket(Long numberOfTicket) {
		this.numberOfTicket = numberOfTicket;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getShowDate() {
		return showDate;
	}

	public void setShowDate(Date showDate) {
		this.showDate = showDate;
	}

	public String getShowTime() {
		return showTime;
	}

	public void setShowTime(String showTime) {
		this.showTime = showTime;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Override
	public String toString() {
		return "Payment [id=" + id + ", movieid=" + movieid + ", numberOfTicket=" + numberOfTicket + ", price=" + price
				+ ", showDate=" + showDate + ", showTime=" + showTime + ", paymentType=" + paymentType + ", totalPrice="
				+ totalPrice + "]";
	}
     
}
     

