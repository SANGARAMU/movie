  package com.jspiders.mavenbasics.entity;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import com.jspiders.mavenbasics.constants.AppConstants;

@Entity
@Table(name = AppConstants.MOVIE_INFO)
public class Movie  implements Serializable
{
	@Id
	@Column(name="id")
      private Long id;
      
	@Column(name="name")
      private String name;
      
	@Column(name="rating")
      private String rating;
      
	@Column(name="budget")
      private Double budget;
      
	@Column(name="release_date")
      private Date releaseDate;
      
      public Movie() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public Double getBudget() {
		return budget;
	}

	public void setBudget(Double budget) {
		this.budget = budget;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
      
      
      
}
