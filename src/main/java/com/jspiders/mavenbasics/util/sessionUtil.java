package com.jspiders.mavenbasics.util;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class sessionUtil 
{
	private static SessionFactory sf=null; 
	
	
	public sessionUtil() { }
	
	public static SessionFactory getInstance()
	{
		if(sf==null) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory=configuration.buildSessionFactory();
		sf=sessionFactory;
		}
		return sf;
		
		
	}
      
}
