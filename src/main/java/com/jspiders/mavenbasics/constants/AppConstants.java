package com.jspiders.mavenbasics.constants;

public interface AppConstants {

	public static String MOVIE_INFO="movie_info";
	
	public static String PAYMENT_INFO="payment_info"; 
	
	public static String COUNTRY_INFO="country_info"; 
	
	public static String PM_INFO="prime_minister_info"; 
	
	public static String COMPANY_INFO="company_info"; 
	
	public static String DEPARTMENT_INFO="department_info"; 
	
	public static String STUDENT_INFO="student_info";
	
	public static String SCHOOL_INFO="school_info";
	
	
}
